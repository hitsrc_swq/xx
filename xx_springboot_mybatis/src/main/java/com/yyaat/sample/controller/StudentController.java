package com.yyaat.sample.controller;

import com.github.pagehelper.PageInfo;
import com.yyaat.sample.pojo.Student;
import com.yyaat.sample.pojo.query.StudentQuery;
import com.yyaat.sample.service.StudentService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class StudentController {

    @Autowired
    private StudentService studentService;

    //显示
    @GetMapping("/homePage")
    public String index(Model model, StudentQuery studentQuery) {
        PageInfo<Student> studentPageInfo = studentService.listStudentByName(studentQuery);
        model.addAttribute("page", studentPageInfo);
        return "crud/content";
    }

    @PostMapping("/homePage")
    public String listStudentByName(Model model, StudentQuery studentQuery) {
        PageInfo<Student> studentPageInfo = studentService.listStudentByName(studentQuery);
        model.addAttribute("page", studentPageInfo);
        return "crud/content";
    }

    //删除
    @GetMapping("/delete/{id}")
    public String deleteStudentById(@PathVariable("id") Integer id, RedirectAttributes attributes) {
        Boolean flag = studentService.deleteStudentById(id);
        if (flag) {
            attributes.addFlashAttribute("message", "删除用户成功");
            return "redirect:/homePage";
        } else {
            attributes.addFlashAttribute("message", "删除用户失败");
            return "redirect:/homePage";
        }
    }

    //修改
    @GetMapping("/edit/{id}")
    public String toEdit(@PathVariable("id") Integer id, Model model) {
        model.addAttribute("student", studentService.queryStudentById(id));
        return "crud/editStudent";
    }

    @PostMapping("/edit")
    public String editStudent(Student student, RedirectAttributes attributes, StudentQuery studentQuery) {
        //判断修改的用户名是否已存在
        student.setName(student.getName());
        PageInfo<Student> studentPageInfo = studentService.listStudentByNameOne(studentQuery);
        System.out.println("studentPageInfo.getSize()=>" + studentPageInfo.getSize());

        if (studentPageInfo.getSize() <= 1) {
            boolean flag = studentService.updateStudent(student);
            if (flag) {
                attributes.addFlashAttribute("message", "修改用户成功");
                return "redirect:/homePage";
            } else {
                attributes.addFlashAttribute("message", "修改用户失败");
                return "redirect:/homePage";
            }
        } else {
            attributes.addFlashAttribute("message", "用户名 [ " + student.getName() + " ] 已存在");
            return "redirect:/edit/" + student.getId();
        }
    }

    //添加
    @GetMapping("/update")
    public String toUpdate(Model model) {
        Student student = new Student();
        model.addAttribute("student", student);
        return "crud/addStudent";
    }

    @PostMapping("/add")
    public String addStudent(Student student, RedirectAttributes attributes, StudentQuery studentQuery) {
        //判断修改的用户名是否已存在
        student.setName(student.getName());
        PageInfo<Student> studentPageInfo = studentService.listStudentByNameOne(studentQuery);
        System.out.println("studentPageInfo.getSize()=>" + studentPageInfo.getSize());

        if (studentPageInfo.getSize() <= 1) {
            boolean flag = studentService.addStudent(student);
            if (flag) {
                attributes.addFlashAttribute("message", "新增用户成功");
                return "redirect:/homePage";
            } else {
                attributes.addFlashAttribute("message", "新增用户失败");
                return "redirect:/homePage";
            }
        } else {
            attributes.addFlashAttribute("message", "用户名 [ " + student.getName() + " ] 已存在");
            return "redirect:/update";
        }

    }
}
