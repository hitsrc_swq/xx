package com.yyaat.sample.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import com.yyaat.sample.pojo.Student;
import com.yyaat.sample.pojo.query.StudentQuery;

@Mapper
@Repository
public interface StudentDao {
    //查询所有信息
    public List<Student> listStudent();

    //根据用户名查询并分页显示
    public List<Student> listStudentByName(StudentQuery studentQuery);

    // 根据id删除
    public int deleteStudentById(Integer id);

    //根据id查询
    public Student queryStudentById(Integer id);

    //修改
    public int updateStudent(Student student);

    //通过名字精准查询
    public List<Student> listStudentByNameOne(StudentQuery studentQuery);

    //新增
    public int addStudent(Student student);
}
