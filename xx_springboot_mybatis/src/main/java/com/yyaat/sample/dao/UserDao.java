package com.yyaat.sample.dao;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import com.yyaat.sample.pojo.User;

@Mapper
@Repository
public interface UserDao {

    //登录
    public User login(User user);

    //注册
    public int register(User user);

    //检查
    public User exitUsername(User user);
}
