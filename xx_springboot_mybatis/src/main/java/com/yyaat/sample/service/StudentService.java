package com.yyaat.sample.service;

import com.github.pagehelper.PageInfo;
import com.yyaat.sample.pojo.Student;
import com.yyaat.sample.pojo.query.StudentQuery;

import java.util.List;

public interface StudentService {
    //查询所有信息
    public List<Student> listStudent();

    //根据用户名查询并分页显示
    public PageInfo<Student> listStudentByName(StudentQuery studentQuery);

    // 根据id删除
    public boolean deleteStudentById(Integer id);

    //根据id查询用户
    public Student queryStudentById(Integer id);

    //修改用户
    public boolean updateStudent(Student student);

    //通过姓名精准查询
    public PageInfo<Student> listStudentByNameOne(StudentQuery studentQuery);

    //新增
    public boolean addStudent(Student student);
}
