package com.yyaat.sample.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.yyaat.sample.dao.StudentDao;
import com.yyaat.sample.pojo.Student;
import com.yyaat.sample.pojo.query.StudentQuery;
import com.yyaat.sample.service.StudentService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentServiceImpl implements StudentService {

    @Autowired
    private StudentDao studentDao;

    @Override
    public List<Student> listStudent() {
        return studentDao.listStudent();
    }

    @Override
    public PageInfo<Student> listStudentByName(StudentQuery studentQuery) {
        PageHelper.startPage(studentQuery.getPageNum(), studentQuery.getPageSize());
        return new PageInfo<Student>(studentDao.listStudentByName(studentQuery));
    }

    @Override
    public boolean deleteStudentById(Integer id) {
        return studentDao.deleteStudentById(id) > 0 ? true : false;
    }

    @Override
    public Student queryStudentById(Integer id) {
        return studentDao.queryStudentById(id);
    }

    @Override
    public boolean updateStudent(Student student) {
        return studentDao.updateStudent(student) > 0 ? true : false;
    }

    @Override
    public PageInfo<Student> listStudentByNameOne(StudentQuery studentQuery) {
        PageHelper.startPage(studentQuery.getPageNum(), studentQuery.getPageSize());
        return new PageInfo<Student>(studentDao.listStudentByName(studentQuery));
    }

    @Override
    public boolean addStudent(Student student) {
        return studentDao.addStudent(student) > 0 ? true : false;
    }
}
