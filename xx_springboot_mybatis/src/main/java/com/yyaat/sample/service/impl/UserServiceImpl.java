package com.yyaat.sample.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yyaat.sample.dao.UserDao;
import com.yyaat.sample.pojo.User;
import com.yyaat.sample.service.UserService;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    @Override
    public boolean login(User user) {
        System.out.println("Impl 登录");
        return userDao.login(user) == null ? false : true;
    }

    @Override
    public boolean register(User user) {
        System.out.println("Impl 注册");
        return userDao.register(user) > 0 ? true : false;
    }

    @Override
    public boolean exitUsername(User user) {
        System.out.println("Impl 检查");
        return userDao.exitUsername(user) == null ? false : true;
    }
}
