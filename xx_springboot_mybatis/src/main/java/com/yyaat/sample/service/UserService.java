package com.yyaat.sample.service;

import com.yyaat.sample.pojo.User;

public interface UserService {
    //登录
    public boolean login(User user);

    //注册
    public boolean register(User user);

    //检查
    public boolean exitUsername(User user);
}
